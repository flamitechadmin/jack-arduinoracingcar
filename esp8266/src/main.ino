/*
A simple socket server which:
  + Connect to a WiFi, and listen on specified PORT
  + Transport all the receive commands to Serial
  + Blink the LED to indicate the connection status
*/
#include <ESP8266WiFi.h>

#define OUTPUT Serial
#define PORT 2311
#define LED 0
#define BAUD 9600

const char* ssid = "HomeLau1";
const char* pwd = "440tansonnhi";
// const char* ssid = "DOU_Networks (SCS)";
// const char* pwd = "DOU12345";

WiFiServer server(PORT);

void setup(){
  OUTPUT.begin(BAUD);
  OUTPUT.flush();

  OUTPUT.print("[LOG]Starting...");
  OUTPUT.print("[LOG]Connecting to WiFi{");
  OUTPUT.print(ssid);
  OUTPUT.println("}...");
  WiFi.begin(ssid, pwd);

  pinMode(LED, OUTPUT);

  while(WiFi.status() != WL_CONNECTED){
    OUTPUT.println("[LOG].");
    digitalWrite(LED, HIGH);
    delay(250);
    digitalWrite(LED, LOW);
    delay(250);
  }

  OUTPUT.print("[LOG] Wifi Connected: ");
  OUTPUT.println(WiFi.localIP());
  digitalWrite(LED, HIGH);

  server.begin();
  OUTPUT.println("[LOG] Server started");
}

void blinkFast(){
  for (int i = 0; i < 5; i++){
    digitalWrite(LED, HIGH);
    delay(20);
    digitalWrite(LED, LOW);
    delay(20);
  }
  digitalWrite(LED, HIGH);
}

void loop(){
  WiFiClient client = server.available();
  if (!client){
    return;
  }
  OUTPUT.println("[LOG]New client connected...");

  String req;
  do{
    //Wait until client send some data
    while(!client.available()){
      delay(1);
    }

    req = client.readStringUntil('\r');
    if (req.equalsIgnoreCase("exit"))
      break;
    OUTPUT.println(req);
    client.flush();
    blinkFast();

  }while(1);

  digitalWrite(LED, HIGH);
  OUTPUT.println("[LOG]Client disconnected");
}
