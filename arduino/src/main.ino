#include <Servo.h>
#include <SoftwareSerial.h>

/******************************************************************
SERVO CONSTANTS
******************************************************************/
#define SERVO_PORT 9
#define MAX_SERVO_ANGLE 45
#define SERVO_START_ANGLE 90
#define SERVO_DELAY 20

#define SERIAL_RX 10
#define SERIAL_TX 11
#define ESP_SERIAL_BAUD 9600

const bool DEBUG = true;
const char* PREFIX_LOG = "[LOG]";
const char* PREFIX_SERVO = "Servo:";
const char* PREFIX_MOTOR = "Motor:";
const char* PREFIX_LED = "LED:";
const char SEPARATOR = '$';

/******************************************************************
MAIN CODE
******************************************************************/
Servo servo;
SoftwareSerial softSerial(SERIAL_RX, SERIAL_TX);
int servoAngle = 0;

void setup(){
  setupServo();
  setupSerials();

  logln("Finished setup everything!");
}

void loop(){
  waitForCommands();
  delay(100);
}
/******************************************************************
LOGGING METHODS
******************************************************************/
//If debug, print log message to hardware serial
void logln(String msg){
  if (DEBUG){
    Serial.println(msg);
  }
}

/******************************************************************
SERIAL CONTROLLING CODE
******************************************************************/
/* If debug, use hardware serial to log. Use software serial to
talk to ESP8266 */
void setupSerials(){
  if (DEBUG){
    Serial.begin(115200);
    softSerial.begin(ESP_SERIAL_BAUD);
  }else{
    Serial.begin(ESP_SERIAL_BAUD);
  }
}

void waitForCommands(){
  String cmd = "";
  int len = 0;
  while(softSerial.available() > 0){
    char ch = (char)softSerial.read();
    if (ch == '\n')
      break;
    cmd += ch;
    len++;
  }

  //If there's a command to execute, execute it.
  //Otherwise, log it out
  if (len > 0){
    handleCommand(cmd);
  }
}

void handleCommand(String cmd){
  int logIndex = cmd.indexOf(PREFIX_LOG);
  //This is not a command, it's a log message from ESP8266
  if (logIndex == 0){
    logln(cmd);
  }else{
    if (cmd.indexOf(PREFIX_SERVO) != -1){
      logln("[ArLOG] Handle servo cmd");
      handleServoCmd(cmd);
    }else if (cmd.indexOf(PREFIX_MOTOR) != -1){
      logln("[ArLOG] Handle motor cmd");
      handleMotorCmd(cmd);
    }else if (cmd.indexOf(PREFIX_LED) != -1){
      logln("[ArLOG] Handle LED cmd");
      handleLEDCmd(cmd);
    }
  }
}

void handleServoCmd(String cmd){
  // int index = cmd.indexOf(PREFIX_SERVO) + 6;
  // cmd = cmd.substring(index);
  // String tmp = extractString(cmd, SEPARATOR);
  // int tmpInt = tmp.toInt();
  // if (tmpInt == 0){
  //   stopTurn();
  // }else if (tmpInt > 0){
  //   turnLeft(tmpInt);
  // }else{
  //   turnRight(tmpInt);
  // }
}

void handleMotorCmd(String cmd){

}

void handleLEDCmd(String cmd){
  int index = cmd.indexOf(PREFIX_LED) + 4;
  cmd = cmd.substring(index);
  index = cmd.indexOf(SEPARATOR);
  String tmp = cmd.substring(0, index);
  int red = tmp.toInt();
  if (DEBUG)
    logln("Red: raw=" + tmp + ", int= " + String(red));
  cmd = cmd.substring(index + 1);

  index = cmd.indexOf(SEPARATOR);
  tmp = cmd.substring(0, index);
  int green = tmp.toInt();
  if (DEBUG)
    logln("Green: raw=" + tmp + ", int= " + String(green));
  cmd = cmd.substring(index + 1);

  index = cmd.indexOf(SEPARATOR);
  tmp = cmd.substring(0, index);
  int blue = tmp.toInt();
  if (DEBUG)
    logln("Blue: raw=" + tmp + ", int= " + String(blue));
  cmd = cmd.substring(index + 1);
}

/******************************************************************
LED CONTROLLING CODE
******************************************************************/

void changeLEDColor(int r, int g, int b){
  String log = "LED: " + String(r) + ", " + String(g) + ", " + String(b);
  logln(log); //TODO
}

/******************************************************************
SERVO CONTROLLING CODE
******************************************************************/
void setupServo(){
  servo.attach(SERVO_PORT);
}

// Turn the car to right
// percent: 0 - 100
void turnRight(int percent){
  servo.write(SERVO_START_ANGLE + percentOf(MAX_SERVO_ANGLE, percent));
  delay(SERVO_DELAY);
}

// Turn the car to left
// percent: 0 - 100
void turnLeft(int percent){
  servo.write(SERVO_START_ANGLE - percentOf(MAX_SERVO_ANGLE, percent));
  delay(SERVO_DELAY);
}

//Put the car to straight line
void stopTurn(){
  servo.write(SERVO_START_ANGLE);
  delay(SERVO_DELAY);
}

void demoServo(){
  stopTurn();
  delay(1000);
  for (int i = 0; i <= 100; i++){
    turnRight(i);
  }
  stopTurn();
  delay(1000);
  for (int i = 0; i <= 100; i++){
    turnLeft(i);
  }
  stopTurn();
  delay(1000);
}
/******************************************************************
COMMON UTIL FUNCTIONS
******************************************************************/
int percentOf(int total, int percent){
  return percent * total / 100;
}
