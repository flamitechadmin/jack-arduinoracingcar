package com.pttruc87.rcarduino.events;

/**
 * Created by truc.pham on 3/26/16.
 */
public class SocketConnectionEvent {
    public String mMessage;

    public SocketConnectionEvent(String message) {
        mMessage = message;
    }
}
