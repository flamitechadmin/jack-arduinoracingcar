package com.pttruc87.rcarduino.connection;

import android.graphics.Color;

import java.util.Locale;

/**
 * Created by truc.pham on 3/30/16.
 */
public class CommandBuilder {
    private static final String CMD_CHANGE_SPEED = "MOTOR:";
    private static final String CMD_TURN_LEFT_RIGHT = "SERVO:";
    private static final String CMD_LOW_LED = "LED:";

    public static final String buildChangeSpeedCommand(int percent) {
        return String.format(Locale.ENGLISH, "%s%d", CMD_CHANGE_SPEED, percent);
    }

    public static final String buildLeftRightCommand(int percent) {
        return String.format(Locale.ENGLISH, "%s%d", CMD_TURN_LEFT_RIGHT, percent);
    }

    public static final String buildLowLEDCommand(int color) {
        int r = Color.red(color);
        int g = Color.green(color);
        int b = Color.blue(color);
        return String.format(Locale.ENGLISH, "%s%d$%d$%d$", CMD_LOW_LED, r, g, b);
    }
}
