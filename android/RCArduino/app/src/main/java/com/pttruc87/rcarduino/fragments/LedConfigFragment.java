package com.pttruc87.rcarduino.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.larswerkman.holocolorpicker.ColorPicker;
import com.pttruc87.rcarduino.R;
import com.pttruc87.rcarduino.events.CloseFragmentEvent;
import com.pttruc87.rcarduino.events.ColorChangedEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by truc.pham on 3/25/16.
 */
public class LedConfigFragment extends Fragment implements ColorPicker.OnColorSelectedListener {


    @Bind(R.id.root_led_config)
    CoordinatorLayout mRootLedConfig;

    @Bind(R.id.color_picker)
    ColorPicker mColorPicker;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_led_config, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    public static LedConfigFragment newInstance(int color) {
        LedConfigFragment result = new LedConfigFragment();
        Bundle args = new Bundle();
        args.putInt("initial led color", color);
        result.setArguments(args);
        return result;
    }

    @OnClick(R.id.root_led_config)
    public void onBackgroundClicked(View view) {
        EventBus.getDefault().post(new CloseFragmentEvent());
    }

    @Override
    public void onResume() {
        super.onResume();
        mColorPicker.setOnColorSelectedListener(this);
        int previousColor = Color.BLACK;
        Bundle args = getArguments();
        if (args != null)
            previousColor = args.getInt("initial led color", Color.BLACK);
        mColorPicker.setColor(previousColor);
    }

    @Override
    public void onColorSelected(int color) {
        EventBus.getDefault().post(new ColorChangedEvent(color));
    }
}
