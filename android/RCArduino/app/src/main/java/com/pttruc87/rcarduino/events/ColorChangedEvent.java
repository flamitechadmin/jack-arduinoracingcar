package com.pttruc87.rcarduino.events;

/**
 * Created by truc.pham on 3/25/16.
 */
public class ColorChangedEvent {
    public int mColor;

    public ColorChangedEvent(int color) {
        mColor = color;
    }
}
