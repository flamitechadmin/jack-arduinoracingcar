package com.pttruc87.rcarduino.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.pttruc87.rcarduino.R;

/**
 * This {@link ImageView} will scale the Bitmap to make sure it will cover the View regardless of Bitmap size and View size.<br>
 * You can use {@link MinFittedImageView#setDockSide(int)} to select the side at which the scaled bitmap will dock, 0: left, 1: top, 2: right, 3: bottom
 * Created by truc.pham on 3/17/16.
 */
public class MinFittedImageView extends ImageView {
    private float mRatio2Fit = 1f;
    private float mPx, mPy;
    private int mDockSide = 0;
    private Point mBitmapSize;

    public MinFittedImageView(Context context) {
        super(context);
        parseAttributes(context, null);
    }

    public MinFittedImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseAttributes(context, attrs);
    }

    public MinFittedImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttributes(context, attrs);
    }

    private void parseAttributes(Context context, AttributeSet attributeSet) {
        if (attributeSet == null) {
            mDockSide = 0;
        } else {
            TypedArray array = context.obtainStyledAttributes(attributeSet, R.styleable.MinFittedImageView);
            mDockSide = array.getInt(R.styleable.MinFittedImageView_dockSide, 1);
            array.recycle();
        }
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        saveBitmapSize();
        invalidate();
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        saveBitmapSize();
        invalidate();
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        saveBitmapSize();
        invalidate();
    }

    public void setDockSide(int value) {
        mDockSide = value;
        requestLayout();
    }

    private void saveBitmapSize() {
        mBitmapSize = null;
        Drawable drawable = getDrawable();
        if (drawable != null && drawable instanceof BitmapDrawable) {
            Bitmap bmp = ((BitmapDrawable) drawable).getBitmap();
            if (bmp != null && !bmp.isRecycled()) {
                mBitmapSize = new Point(bmp.getWidth(), bmp.getHeight());
            }
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        updateRatio(w, h);
        invalidate();
    }

    private void updateRatio(int width, int height) {
        if (mBitmapSize == null) {
            mRatio2Fit = 1f;
            mPx = 0f;
            mPy = 0f;
        } else {
            mRatio2Fit = Math.max((float) mBitmapSize.x / width, (float) mBitmapSize.y / height);
            mPx = width / 2f;
            mPy = height / 2f;
            switch (mDockSide) {
                case 1: {
                    mPy = 0f;
                    break;
                }
                case 2: {
                    mPx = width;
                    break;
                }
                case 3: {
                    mPy = height;
                    break;
                }
            }
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        canvas.scale(mRatio2Fit, mRatio2Fit, mPx, mPy);
        super.onDraw(canvas);
        canvas.restore();
    }
}
