package com.pttruc87.rcarduino;

import android.app.Application;

import com.tumblr.remember.Remember;

/**
 * Created by truc.pham on 3/25/16.
 */
public class RCBuggoApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Remember.init(this, getPackageName());
    }
}
