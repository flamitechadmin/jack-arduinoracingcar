package com.pttruc87.rcarduino.connection;

import android.text.TextUtils;

import com.pttruc87.rcarduino.events.SocketConnectionEvent;
import com.pttruc87.rcarduino.events.SocketOpenedEvent;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by truc.pham on 3/25/16.
 */
public class SocketOutput implements APIOutput {
    private static final int DEFAULT_PORT = 2311;

    private Socket mSocket;
    private OutputStream mSocketOutStream;
    private boolean mExit = false;
    private boolean mStarted = false;
    private String mIPAddress;
    private int mPort;
    private Thread mBGThread;
    private String mWaitingOp;

    public SocketOutput(String ipAddress) {
        this(ipAddress, DEFAULT_PORT);
    }

    public SocketOutput(String ipAddress, int port) {
        mIPAddress = ipAddress;
        mExit = false;
        mStarted = false;
        mPort = port;
    }

    @Override
    public void changeSpeed(int percent) {
        mWaitingOp = CommandBuilder.buildChangeSpeedCommand(percent);
        mBGThread.interrupt();
    }

    @Override
    public void turnLeftRight(int percent) {
        mWaitingOp = CommandBuilder.buildLeftRightCommand(percent);
        mBGThread.interrupt();
    }

    @Override
    public void lowLedMod(int color) {
        mWaitingOp = CommandBuilder.buildLowLEDCommand(color);
        mBGThread.interrupt();
    }

    @Override
    public void start() {
        if (!mStarted) {
            mBGThread = new Thread(mBGRunnable);
            mBGThread.start();
            mStarted = true;
        }
    }

    @Override
    public void shutDown() {
        if (mStarted) {
            mExit = true;
            mBGThread.interrupt();

            try {
                if (mSocket != null)
                    mSocket.close();
                mSocket = null;
                if (mSocketOutStream != null)
                    mSocketOutStream.close();
                mSocketOutStream = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private Runnable mBGRunnable = new Runnable() {
        @Override
        public void run() {
            while (true) {
                if (mExit)
                    break;

                try {
                    if (mSocket == null) {
                        mSocket = new Socket(mIPAddress, mPort);
                    }

                    if (mSocketOutStream == null) {
                        mSocketOutStream = mSocket.getOutputStream();
                        mSocketOutStream.flush();
                    }
                    EventBus.getDefault().post(new SocketOpenedEvent(true, "Connection established"));
                } catch (IOException e) {
                    e.printStackTrace();
                    EventBus.getDefault().post(new SocketOpenedEvent(false, e.getMessage()));
                }

                if (mSocketOutStream != null && !TextUtils.isEmpty(mWaitingOp)) {
                    byte[] data = mWaitingOp.getBytes();
                    try {
                        mSocketOutStream.write(data);
                        mSocketOutStream.flush();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                        EventBus.getDefault().post(new SocketConnectionEvent(ex.getMessage()));
                    }
                }
                mWaitingOp = null;

                try {
                    mBGThread.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };
}
