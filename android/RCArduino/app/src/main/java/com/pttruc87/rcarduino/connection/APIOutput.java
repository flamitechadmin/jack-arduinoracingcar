package com.pttruc87.rcarduino.connection;

/**
 * Created by truc.pham on 3/25/16.
 */
public interface APIOutput {
    void changeSpeed(int percent);

    void turnLeftRight(int percent);

    void lowLedMod(int color);

    void start();

    void shutDown();
}
