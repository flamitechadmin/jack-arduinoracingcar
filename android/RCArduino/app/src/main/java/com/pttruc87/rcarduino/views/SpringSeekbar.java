package com.pttruc87.rcarduino.views;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringListener;
import com.facebook.rebound.SpringSystem;
import com.pttruc87.rcarduino.R;

/**
 * Created by truc.pham on 3/18/16.
 */
public class SpringSeekbar extends View implements SpringListener {
    private Spring mSpring;
    private SpringSystem mSpringSystem;
    private int mKnobResId = R.drawable.blue_circle;
    private int mBackgroundResId = R.drawable.border_blue_bg2;
    private Drawable mKnobDrawable, mBgDrawable;
    private int mValue = 0, mMinValue = -100, mMaxValue = 100;
    private boolean mInvalidateSize = true;
    private boolean mInvalidatePosition = true;
    private int mKnobSize;
    private int mKnobStart;
    private int viewSize; //Either width or height
    private int mDelta;
    private int mValueAtSpring;
    private boolean mSpringInAction = false;
    private boolean mHorizontalMode = true;
    private OnValueChanged mCallback;

    public SpringSeekbar(Context context) {
        super(context);
        parseXMLAttributes(context, null);
        initialize(context);
    }

    public SpringSeekbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        parseXMLAttributes(context, attrs);
        initialize(context);
    }

    public SpringSeekbar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseXMLAttributes(context, attrs);
        initialize(context);
    }

    private void parseXMLAttributes(Context context, AttributeSet attrSet) {
        if (attrSet == null) {
            loadDefaults();
        } else {
            //Parse XML attributes here
            TypedArray array = context.obtainStyledAttributes(attrSet, R.styleable.SpringSeekbar);
            mHorizontalMode = array.getInt(R.styleable.SpringSeekbar_mode, 0) == 0;
            array.recycle();
        }
    }

    private void loadDefaults() {
        mHorizontalMode = true;
    }

    private void initialize(Context context) {
        mDelta = Math.abs(mMaxValue - mMinValue);
        mSpringSystem = SpringSystem.create();
        mSpring = mSpringSystem.createSpring();
        mSpring.setSpringConfig(SpringConfig.fromOrigamiTensionAndFriction(40f, 3f));
        mSpring.addListener(this);

        Resources res = context.getResources();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            mKnobDrawable = res.getDrawable(mKnobResId, null);
            mBgDrawable = res.getDrawable(mBackgroundResId, null);
        } else {
            mKnobDrawable = res.getDrawable(mKnobResId);
            mBgDrawable = res.getDrawable(mBackgroundResId);
        }
    }

    public void setOnValueChanged(OnValueChanged callback) {
        mCallback = callback;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w != oldw || h != oldh) {
            mInvalidateSize = true;
            invalidate();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        invalidateSizeAndPosition();
        mBgDrawable.draw(canvas);

        if (mHorizontalMode)
            mKnobDrawable.setBounds(mKnobStart, 0, mKnobSize + mKnobStart, mKnobSize);
        else
            mKnobDrawable.setBounds(0, mKnobStart, mKnobSize, mKnobSize + mKnobStart);
        mKnobDrawable.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (mSpringInAction) return false;
        int action = event.getAction() & MotionEvent.ACTION_MASK;

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                mInvalidatePosition = false;
                invalidate();
                return true;
            }
            case MotionEvent.ACTION_MOVE: {
                float touchPos = mHorizontalMode ? event.getX() : event.getY();
                if (viewSize != 0) {
                    mInvalidatePosition = true;
                    mValue = (int) ((touchPos / viewSize) * mDelta + mMinValue);
                    standardizeValues();
                    if (mCallback != null)
                        mCallback.onValueChanged(this, mValue, mMinValue, mMaxValue);
                }
                invalidate();
                return true;
            }
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL: {
                mInvalidatePosition = true;
                springBackDefault();
                invalidate();
                return true;
            }
        }
        return super.onTouchEvent(event);
    }

    private void standardizeValues() {
        mValue = Math.max(mValue, mMinValue);
        mValue = Math.min(mValue, mMaxValue);
    }

    private void springBackDefault() {
        mValueAtSpring = -mValue;
        mSpring.setCurrentValue(0f);
        mSpringInAction = true;
        mSpring.setEndValue(1f);
    }

    private void invalidateSizeAndPosition() {
        viewSize = mHorizontalMode ? getWidth() : getHeight();
        int viewAnotherSize = mHorizontalMode ? getHeight() : getWidth();
        if (mInvalidateSize && viewSize > 0 && viewAnotherSize > 0) {
            mInvalidateSize = false;
            if (mHorizontalMode)
                mBgDrawable.setBounds(0, 0, viewSize, viewAnotherSize);
            else
                mBgDrawable.setBounds(0, 0, viewAnotherSize, viewSize);
            mKnobSize = viewAnotherSize;
        }

        if (mInvalidatePosition) {
            final int MAX_HOR_POS = viewSize - mKnobSize;
            float delta = mMaxValue - mMinValue;
            if (delta == 0)
                mKnobStart = 0;
            else
                mKnobStart = (int) ((mValue - mMinValue) / delta * MAX_HOR_POS);
            mInvalidatePosition = false;
        }
    }

    @Override
    public void onSpringUpdate(Spring spring) {
        mValue = (int) (spring.getCurrentValue() * mValueAtSpring - mValueAtSpring);
        mInvalidatePosition = true;
        invalidate();
    }

    @Override
    public void onSpringAtRest(Spring spring) {
        mSpringInAction = false;
        mValue = 0;
    }

    @Override
    public void onSpringActivate(Spring spring) {

    }

    @Override
    public void onSpringEndStateChange(Spring spring) {

    }

    public interface OnValueChanged {
        void onValueChanged(SpringSeekbar view, int value, int min, int max);
    }
}