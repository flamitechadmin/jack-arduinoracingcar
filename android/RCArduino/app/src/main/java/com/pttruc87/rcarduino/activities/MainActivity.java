package com.pttruc87.rcarduino.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pttruc87.rcarduino.R;
import com.pttruc87.rcarduino.connection.APIOutput;
import com.pttruc87.rcarduino.connection.SocketOutput;
import com.pttruc87.rcarduino.events.CloseFragmentEvent;
import com.pttruc87.rcarduino.events.ColorChangedEvent;
import com.pttruc87.rcarduino.events.SocketOpenedEvent;
import com.pttruc87.rcarduino.fragments.LedConfigFragment;
import com.pttruc87.rcarduino.views.SpringSeekbar;
import com.tumblr.remember.Remember;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements SpringSeekbar.OnValueChanged {
    @Bind(R.id.root_container)
    CoordinatorLayout mRootContainer;

    @Bind(R.id.vertical_springer)
    SpringSeekbar mVerticalSpringer;

    @Bind(R.id.horizontal_springer)
    SpringSeekbar mHorizontalSpringer;

    @Bind(R.id.connection_label)
    TextView mConnectionLabel;

    @Bind(R.id.connection_config)
    Button mConnectionConfig;

    @Bind(R.id.led_config)
    Button mLedConfig;

    @Bind(R.id.ip_address_root)
    RelativeLayout mIPAddressRoot;

    @Bind(R.id.part1)
    TextView mIPAddrPart1;
    @Bind(R.id.part2)
    TextView mIPAddrPart2;
    @Bind(R.id.part3)
    TextView mIPAddrPart3;
    @Bind(R.id.part4)
    TextView mIPAddrPart4;

    private APIOutput mAPIOutput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawable(null);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mVerticalSpringer.setOnValueChanged(this);
        mHorizontalSpringer.setOnValueChanged(this);
        toggleConnectionUI(true);
    }

    @OnClick(R.id.led_config)
    public void onLedConfigClicked(View view) {
        LedConfigFragment fragment = LedConfigFragment.newInstance(Remember.getInt("Led Color", Color.BLACK));
        getSupportFragmentManager().beginTransaction().add(android.R.id.content, fragment, LedConfigFragment.class.getSimpleName()).commit();
    }

    @Override
    public void onBackPressed() {
        if (!closeLedConfigFragment()) {
            super.onBackPressed();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handle(CloseFragmentEvent event) {
        closeLedConfigFragment();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handle(ColorChangedEvent event) {
        if (mAPIOutput != null) {
            Remember.putInt("Led Color", event.mColor);
            mLedConfig.setBackgroundColor(event.mColor);
            mAPIOutput.lowLedMod(event.mColor);
        } else {
            Snackbar.make(mRootContainer, R.string.init_connection_first, Snackbar.LENGTH_SHORT).show();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handle(SocketOpenedEvent event) {
        if (event.isSuccessful) {
            Snackbar.make(mRootContainer, event.mMessage, Snackbar.LENGTH_SHORT).show();
        } else {
            Snackbar.make(mRootContainer, getResources().getString(R.string.connection_error, event.mMessage), Snackbar.LENGTH_LONG).show();
        }

        toggleConnectionUI(!event.isSuccessful);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
        mLedConfig.setBackgroundColor(Remember.getInt("Led Color", Color.BLACK));
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void onDestroy() {
        if (mAPIOutput != null)
            mAPIOutput.shutDown();
        super.onDestroy();
    }

    private boolean closeLedConfigFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(LedConfigFragment.class.getSimpleName());
        if (fragment != null) {
            fragmentManager.beginTransaction().remove(fragment).commit();
            return true;
        } else {
            return false;
        }
    }

    @OnClick(R.id.connection_config)
    public void onConnectionSetupClicked(View view) {
        String ipAddress = getIPAddress();
        if (ipAddress != null) {
            mAPIOutput = new SocketOutput(ipAddress);
            mAPIOutput.start();
        }
    }

    private String getIPAddress() {
        try {
            return String.format(Locale.ENGLISH, "%s.%s.%s.%s",
                    mIPAddrPart1.getText().toString(),
                    mIPAddrPart2.getText().toString(),
                    mIPAddrPart3.getText().toString(),
                    mIPAddrPart4.getText().toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            Snackbar.make(mRootContainer, R.string.invalid_ip_address, Snackbar.LENGTH_SHORT).show();
        }
        return null;
    }

    @Override
    public void onValueChanged(SpringSeekbar view, int value, int min, int max) {
        switch (view.getId()) {
            case R.id.horizontal_springer: {
                onHorizontalValueChanged(value, min, max);
                break;
            }
            case R.id.vertical_springer: {
                onVerticalValueChanged(value, min, max);
                break;
            }
        }
    }

    private void onVerticalValueChanged(int value, int min, int max) {
        Log.d("RC Buggo", "Vertical - value = " + value + ", min = " + min + ", max = " + max);
        if (mAPIOutput != null) {
            int percent = (int) (value * 1000f / (max - min));
            mAPIOutput.changeSpeed(percent);
        }
    }

    private void onHorizontalValueChanged(int value, int min, int max) {
        Log.d("RC Buggo", "Horizontal - value = " + value + ", min = " + min + ", max = " + max);
        if (mAPIOutput != null) {
            int percent = (int) (value * 1000f / (max - min));
            mAPIOutput.turnLeftRight(percent);
        }
    }

    private void toggleConnectionUI(boolean show) {
        mIPAddressRoot.setVisibility(show ? View.VISIBLE : View.GONE);
        mConnectionConfig.setVisibility(show ? View.VISIBLE : View.GONE);

        mConnectionLabel.setTextColor(show ? Color.BLACK : Color.GREEN);
        String tmp;
        if (show) {
            tmp = getResources().getString(R.string.connection_not_established);
        } else {
            tmp = getResources().getString(R.string.connection_established, getIPAddress());
        }
        mConnectionLabel.setText(tmp);
    }
}
