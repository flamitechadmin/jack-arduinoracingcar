package com.pttruc87.rcarduino.events;

/**
 * Created by truc.pham on 3/26/16.
 */
public class SocketOpenedEvent {
    public boolean isSuccessful;
    public String mMessage;

    public SocketOpenedEvent(boolean success, String message) {
        mMessage = message;
        isSuccessful = success;
    }
}
